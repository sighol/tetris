extern crate sdl2;

use sdl2::event::Event;
use sdl2::image::{LoadTexture, INIT_JPG, INIT_PNG};
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::{Canvas, Texture, TextureCreator};
use sdl2::video::{Window, WindowContext};

use std::thread::sleep;
use std::time::Duration;

fn main() {
    let sdl_context = sdl2::init().expect("SDL initialization failed");
    let video_subsystem = sdl_context
        .video()
        .expect("Couldn't get SDL video subsystem");

    let window = video_subsystem
        .window("rust-sdl2 demo: Video", 800, 600)
        .position_centered()
        .opengl()
        .build()
        .expect("Failed to create window");

    let mut canvas = window
        .into_canvas()
        .target_texture()
        .present_vsync()
        .build()
        .expect("Failed to convert window into canvas");

    let texture_creator: TextureCreator<_> = canvas.texture_creator();

    let mut event_pump = sdl_context
        .event_pump()
        .expect("Failed to get SDL event pump");

    sdl2::image::init(INIT_JPG | INIT_PNG).expect("Couldn't initialize image context");

    let image_texture = texture_creator
        .load_texture("assets/my_image.png")
        .expect("Failed to load image");

    let mut b: u8 = 0;

    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                }
                | Event::KeyDown {
                    keycode: Some(Keycode::Q),
                    ..
                } => break 'running,
                _ => {}
            }

            b = b.wrapping_add(2);
            canvas.set_draw_color(Color::RGB(255, 150, b));
            canvas.clear();

            canvas
                .copy(&image_texture, None, None)
                .expect("Render failed");

            canvas.present();

            sleep(Duration::from_millis(16));
        }
    }

    println!("Hello, world!");
}
